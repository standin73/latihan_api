<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show($username){
        
        return new UserResource
            (User::where('username', $username)->select('username','created_at')->first()
        );
    }

    public function activity($username){
        return User::where('username', $username)->with('forums', 'forumcomment')->first();
    }
}
