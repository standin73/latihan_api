<?php

namespace App\Http\Controllers;

use App\ForumComment;
use Illuminate\Http\Request;
use App\Http\Controllers\AuthUserTrait;
use Illuminate\Support\Facades\Validator;


class ForumCommentController extends Controller
{

    use AuthUSerTrait;

    public function __construct()
    {
        return auth()->shouldUse('api');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $forum_id)
    {
        $this->validateRequest();

        $user = $this->getAuthUser();

        $user->forumComment()->create([
            'body' => request('body'),
            'forum_id' => $forum_id
        ]);
        // return respon
        return response()->json(['message' => 'successfully comment posted']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $forumId, $commentId)
    {
        $this->validateRequest();
        $forumcomment = ForumComment::find($commentId);

        $this->checkOwnership($forumcomment->user_id);

        $forumcomment->update([
            'body' => request('body')
        ]);
        // return respon
        return response()->json(['message' => 'successfully comment updated']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($forumId, $commentId)
    {
        $forumcomment = ForumComment::find($commentId);
        $this->checkOwnership($forumcomment->user_id);
        $forumcomment->delete();

        return response()->json(['message' => 'successfully deleted']);
    }

    private function validateRequest()
        {
            $validator  = Validator::make(request()->all(), [
                'body' => 'required|min:5'
            ]);
    
            if($validator->fails()){
                response()->json($validator->messages())->send();
                exit;
            }
        }
}
