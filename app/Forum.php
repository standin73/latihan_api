<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    protected $table = 'forums';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User')->select(['id','username']);
    }

    public function comments()
    {
        return $this->hasMany('App\ForumComment');
    }
}
