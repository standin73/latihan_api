<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumComment extends Model
{
    protected $table = 'forumcomments';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function forum()
    {
        return $this->hasOne('App\Forum');
    }
}
